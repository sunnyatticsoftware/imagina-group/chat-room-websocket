using Chat.Infra.Repository.SqlServer.Entities;
using Microsoft.EntityFrameworkCore;

namespace Chat.Infra.Repository.SqlServer;

public class ChatRoomDbContext
    : DbContext
{
    public ChatRoomDbContext(DbContextOptions<ChatRoomDbContext> options)
        : base(options)
    {
    }

    public DbSet<ChatEntity> Chats { get; set; } = null!;
    public DbSet<MessageEntity> Messages { get; set; } = null!;
    public DbSet<UserEntity> Users { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder
            .Entity<UserEntity>()
            .HasKey(u => u.Username);

        modelBuilder
            .Entity<MessageEntity>()
            .HasOne(m => m.User)
            .WithMany(u => u.Messages)
            .HasForeignKey(m => m.Username);

        modelBuilder
            .Entity<UserEntity>()
            .HasOne(u => u.Chat)
            .WithMany(c => c.Users)
            .HasForeignKey(u => u.ChatId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}