namespace Chat.Infra.Repository.SqlServer.Entities;

public class MessageEntity
{
    public int Id { get; set; }
    public string Text { get; set; } = string.Empty;
    public DateTime CreatedOn { get; set; }
    public string Username { get; set; } = string.Empty;
    
    // Navigation properties
    public UserEntity User { get; set; } = null!;
    
}