namespace Chat.Infra.Repository.SqlServer.Entities;

public class ChatEntity
{
    public Guid Id { get; set; }
    public List<UserEntity> Users { get; set; } = null!;
}