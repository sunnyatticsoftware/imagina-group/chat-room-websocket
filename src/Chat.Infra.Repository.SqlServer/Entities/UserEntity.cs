namespace Chat.Infra.Repository.SqlServer.Entities;

public class UserEntity
{
    public string Username { get; set; } = string.Empty;
    public string ConnectionId { get; set; } = string.Empty;
    public Guid ChatId { get; set; }
    
    // Navigation properties
    public List<MessageEntity> Messages { get; set; } = null!;
    public ChatEntity Chat { get; set; } = null!;
}