using Chat.Application.Contracts;
using Chat.Domain;
using Dapper;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Chat.Infra.Repository.SqlServer;

public class DapperChatRoomRepository
    : IChatRoomRepository
{
    private readonly IDbConnection _dbConnection;
    private readonly IChatRoomFactory _chatRoomFactory;

    public DapperChatRoomRepository(
        SqlConnection sqlConnection,
        IChatRoomFactory chatRoomFactory)
    {
        _dbConnection = sqlConnection;
        _chatRoomFactory = chatRoomFactory;
    }
    
    public async Task<ChatRoom> GetById(Guid id)
    {
        var sqlStatement =
            @"SELECT c.Id, u.Username, u.ConnectionId, m.Id as messageId, m.Text, m.CreatedOn from Chats c 
            LEFT JOIN Users u on u.ChatId = c.Id
            LEFT JOIN Messages m on m.Username = u.Username
            WHERE c.Id = @Id
            ORDER BY messageId";
        
        var queryResults =
            await _dbConnection.QueryAsync<QueryResult>(sqlStatement, new {Id = id});
        var table = queryResults.ToList();
        
        var users = 
            table
                .Where(qr => qr.Username != null)// Leave out null results (it's a left join!)
                .Select(qr => new User(qr.Username!, qr.ConnectionId))
                .DistinctBy(x => x.Username)
                .ToList();
        
        var messages = 
            table
                .Where(qr => qr.MessageId.HasValue) // Leave out null results (it's a left join!)
                .Select(qr => new Message(qr.CreatedOn, qr.Username!, qr.Text))
                .Distinct()
                .OrderBy(x => x.CreatedOn)// Order by created on, in case it fails the order of getting messages on tests
                .ToList();
        
        var chatRoom = _chatRoomFactory.Create(id);
        // Reflection to set internal values
        ReflectionUtil.SetValue(chatRoom, users);
        ReflectionUtil.SetValue(chatRoom, messages);
        return chatRoom;
    }

    public async Task Save(ChatRoom chatRoom)
    {
        var deleteSql = "delete Chats where Id = @Id";
        _ = await _dbConnection.ExecuteAsync(deleteSql, new {Id = chatRoom.Id});

        var insertChatSql = "insert Chats(id) values (@Id)";
        _ = await _dbConnection.ExecuteAsync(insertChatSql, new {Id = chatRoom.Id});

        var chatEntity = Mappers.MapToChatEntity(chatRoom);

        var userParams =
            chatEntity
                .Users
                .Select(x => new
                {
                    Username = x.Username,
                    ConnectionId = x.ConnectionId,
                    ChatId = chatRoom.Id
                });

        var insertUsersSql = "insert Users(Username, ConnectionId, ChatId) values (@Username, @ConnectionId, @ChatId)";
        _ = await _dbConnection.ExecuteAsync(insertUsersSql, userParams);

        var messageParams =
            chatEntity
                .Users
                .SelectMany(x => x.Messages)
                .Select(x => new
                {
                    Username = x.Username,
                    Text = x.Text,
                    CreatedOn = x.CreatedOn
                });
        
        var insertMessagesSql = "insert Messages(Username, Text, CreatedOn) values (@Username, @Text, @CreatedOn)";
        _ = await _dbConnection.ExecuteAsync(insertMessagesSql, messageParams);
    }
    
    internal class QueryResult
    {
        public Guid Id { get; set; }
        public string? Username { get; set; }
        public string ConnectionId { get; set; } = string.Empty;
        public int? MessageId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Text { get; set; } = string.Empty;
    }
}