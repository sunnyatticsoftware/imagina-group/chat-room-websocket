using Chat.Domain;
using Chat.Infra.Repository.SqlServer.Entities;

namespace Chat.Infra.Repository.SqlServer;

public static class Mappers
{
    public static ChatEntity MapToChatEntity(ChatRoom chatRoom)
    {
        var messages = chatRoom.GetAllMessages().ToList();

        var users = (IEnumerable<User>) ReflectionUtil.GetInternalListFieldInfo<User>().GetValue(chatRoom)!;

        var userEntities = new List<UserEntity>();
        foreach (var user in users)
        {
            var username = user.Username;
            var connectionId = user.ConnectionId;
            var userMessages = messages.Where(x => x.CreatedBy == username).ToList();
            var messageEntities =
                userMessages
                    .Select(x => new MessageEntity
                    {
                        Username = username,
                        CreatedOn = x.CreatedOn,
                        Text = x.Text
                    })
                    .ToList();
            
            var userEntity = userEntities.SingleOrDefault(x => x.Username == username) ?? new UserEntity
            {
                Username = username,
                ConnectionId = connectionId,
                Messages = messageEntities
            };
            
            userEntities.Add(userEntity);
        }
        

        return new ChatEntity
        {
            Id = chatRoom.Id,
            Users = userEntities
        };
    }
}