using Chat.Domain;
using System.Reflection;

namespace Chat.Infra.Repository.SqlServer;

public static class ReflectionUtil
{
    public static FieldInfo GetInternalListFieldInfo<T>()
        where T : class
    {
        var fieldInfo =
            typeof(ChatRoom)
                .GetFields(BindingFlags.NonPublic | BindingFlags.Default | BindingFlags.Instance)
                .FirstOrDefault(x => x.FieldType.IsAssignableTo(typeof(IList<T>)));
        if (fieldInfo is null)
        {
            throw new Exception("Cannot extract internal enumerable with reflection");
        }

        return fieldInfo;
    }
    
    public static void SetValue<T>(ChatRoom chatRoom, IList<T> value)
        where T : class
    {
        GetInternalListFieldInfo<T>().SetValue(chatRoom, value);
    }
}