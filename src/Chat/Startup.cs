using Chat.Application;
using Chat.Application.Contracts;
using Chat.Domain.Contracts;
using Chat.Extensions;
using Chat.Infra.Repository.InMemory;
using Chat.Infra.Repository.SqlServer;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Chat;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public void ConfigureServices(IServiceCollection services)
    {
        var connectionString = _configuration.GetValue<string>("ConnectionStrings:ChatsDatabase");
        services
            .AddTransient<ChatCommandService>()
            .AddTransient<ChatQueryService>()
            //.AddSingleton<IChatRoomRepository, InMemoryChatRoomRepository>()
            .AddDbContext<ChatRoomDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            })
            .AddTransient<IChatRoomFactory, ChatRoomFactory>()
            .AddSingleton<IDateTimeFactory, DateTimeFactory>()
            .AddSingleton<IIdFactory, IdFactory>()
            .AddTransient<INotificationService>(sp => new NotificationService(NotificationServiceConfiguration.Default))
            .AddTransient(sp =>
            {
                var sqlConnection = new SqlConnection(connectionString);
                return sqlConnection;
            })
            .AddTransient<IChatRoomRepository, DapperChatRoomRepository>()
            .AddOpenApi()
            .AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseRouting();
        app.UseOpenApi();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}