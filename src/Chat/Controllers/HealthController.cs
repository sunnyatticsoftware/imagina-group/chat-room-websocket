using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("[controller]")]
public class HealthController
    : ControllerBase
{
    private readonly IWebHostEnvironment _environment;

    public HealthController(IWebHostEnvironment environment)
    {
        _environment = environment;
    }
    
    [HttpGet]
    public IActionResult Get()
    {
        var environmentName = _environment.EnvironmentName;
        var assemblyName = typeof(Startup).Assembly.GetName();
        var result = $"Assembly={assemblyName}, Environment={environmentName}";
        return Ok(result);
    }
}