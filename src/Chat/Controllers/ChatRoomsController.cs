using Chat.Application;
using Chat.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("api/[controller]")]
public class ChatRoomsController
    : ControllerBase
{
    private readonly ChatCommandService _chatCommandService;
    private readonly ChatQueryService _chatQueryService;

    public ChatRoomsController(
        ChatCommandService chatCommandService,
        ChatQueryService chatQueryService)
    {
        _chatCommandService = chatCommandService;
        _chatQueryService = chatQueryService;
    }
    
    [HttpPost]
    public async Task<IActionResult> CreateChatRoom()
    {
        var id = await _chatCommandService.Create();
        return Ok(id);
    }
    
    [HttpPost("{id:guid}/users")]
    public async Task<IActionResult> Join(Guid id, [FromBody]UserDto user)
    {
        await _chatCommandService.Join(id, user);
        return Ok();
    }
    
    [HttpPost("{id:guid}/users/{username}/messages")]
    public async Task<IActionResult> SendMessage(Guid id, string username, [FromBody]string text)
    {
        await _chatCommandService.SendMessage(id, username, text);
        return Ok();
    }
    
    [HttpGet("{id:guid}/messages")]
    public async Task<IEnumerable<MessageDto>> GetMessages(Guid id)
    {
        var messages = await _chatQueryService.GetAllMessages(id);
        return messages;
    }
}