using Chat.Application.Contracts;
using Chat.Domain;
using System.Collections.Concurrent;

namespace Chat.Infra.Repository.InMemory;

public class InMemoryChatRoomRepository
    : IChatRoomRepository
{
    private readonly ConcurrentDictionary<Guid, ChatRoom> _chatRooms = new();
    
    public Task<ChatRoom> GetById(Guid id)
    {
        var isFound = _chatRooms.TryGetValue(id, out var chatRoom);
        if (!isFound || chatRoom is null)
        {
            throw new KeyNotFoundException($"Could not find chat room with Id {id}");
        }

        return Task.FromResult(chatRoom);
    }

    public Task Save(ChatRoom chatRoom)
    {
        _chatRooms[chatRoom.Id] = chatRoom;
        return Task.CompletedTask;
    }
}