namespace Chat.Application.Models;

public class UserDto
{
    /// <summary>
    /// This is the username
    /// </summary>
    /// <example>pepe</example>
    public string Username { get; }
    
    /// <summary>
    /// This is the connection Id
    /// </summary>
    /// <example>abc123</example>
    public string ConnectionId { get; }

    public UserDto(string username, string connectionId)
    {
        Username = username;
        ConnectionId = connectionId;
    }
}