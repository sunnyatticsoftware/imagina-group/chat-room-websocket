namespace Chat.Application.Contracts;

public interface IIdFactory
{
    Guid Create();
}