namespace Chat.Domain;

public class Message
{
    public DateTime CreatedOn { get; }
    public string CreatedBy { get; }
    public string Text { get; }

    public Message(DateTime createdOn, string createdBy, string text)
    {
        CreatedOn = createdOn;
        CreatedBy = createdBy;
        Text = text;
    }

    public override string ToString()
    {
        var result = $"[{CreatedOn.ToString("yyyy-MM-dd HH:mm:ss")}] {CreatedBy} says: {Text}";
        return result;
    }
}