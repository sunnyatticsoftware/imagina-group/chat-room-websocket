namespace Chat.Domain.Contracts;

public interface INotificationService
{
    Task SendNotification(string connectionId, string message);
}