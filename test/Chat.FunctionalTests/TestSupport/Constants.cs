namespace Chat.FunctionalTests.TestSupport;

public static class Constants
{
    public static class WebsocketTester
    {
        public const string HostAndPort = "websocket-tester:8080";
    }
}