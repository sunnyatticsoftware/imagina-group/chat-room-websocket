using Chat.Domain.Contracts;
using Chat.Infra.Repository.SqlServer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System.Text;

namespace Chat.FunctionalTests.TestSupport;

public abstract class FunctionalTest
    : IDisposable
{
    private readonly IServiceProvider _serviceProvider;
    private readonly string _uniqueDatabaseName;
    protected HttpClient HttpClient { get; }
    
    public FunctionalTest()
    {
        var server =
            new TestServer(
                new WebHostBuilder()
                    .UseStartup<Startup>()
                    .UseEnvironment("Test")
                    .UseCommonConfiguration()
                    .ConfigureTestServices(ConfigureTestServices));

        _serviceProvider = server.Services;
        HttpClient = server.CreateClient();

        _uniqueDatabaseName = $"Test-{Guid.NewGuid()}";
        using var dbContext = _serviceProvider.CreateScope().ServiceProvider.GetRequiredService<ChatRoomDbContext>();
        dbContext.Database.Migrate();
    }

    protected virtual void ConfigureTestServices(IServiceCollection services)
    {
        // services.Replace<INotificationService>(sp =>
        // {
        //     var notificationServiceMock = new Mock<INotificationService>();
        //     return notificationServiceMock.Object;
        // }, ServiceLifetime.Singleton);
        services.Replace<INotificationService>(sp =>
            {
                var notificationServiceConfiguration = new NotificationServiceConfiguration {Host = $"http://{Constants.WebsocketTester.HostAndPort}" };
                var notificationService = new NotificationService(notificationServiceConfiguration);
                return notificationService;
            },
            ServiceLifetime.Transient);
        
        // Remove EF Core registration
        var dbContextDescriptor =
            services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ChatRoomDbContext>));
        if (dbContextDescriptor != null)
        {
            services.Remove(dbContextDescriptor);
        }

        services
            .AddDbContext<ChatRoomDbContext>((sp, options) =>
            {
                var uniqueDbTestConnectionString = GetUniqueConnectionString(sp);
                options.UseSqlServer(uniqueDbTestConnectionString);
            });
        
        // remove Dapper SqlConnection's registration
        var sqlConnectionDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(SqlConnection));
        if (sqlConnectionDescriptor != null)
        {
            services.Remove(sqlConnectionDescriptor);
        }
        
        services.AddTransient(
            sp =>
            {
                var uniqueDbTestConnectionString = GetUniqueConnectionString(sp);
                var sqlConnection = new SqlConnection(uniqueDbTestConnectionString);
                return sqlConnection;
            });
    }

    private string GetUniqueConnectionString(IServiceProvider sp)
    {
        var configuration = sp.GetRequiredService<IConfiguration>();
        var testConnectionString = configuration.GetValue<string>("ConnectionStrings:ChatsDatabase");
        var parts = testConnectionString.Split(";");
        var uniqueDbTestConnectionStringBuilder = new StringBuilder();
        foreach (var part in parts)
        {
            var isDatabasePart = part.StartsWith("Database=");
            uniqueDbTestConnectionStringBuilder.Append(isDatabasePart
                ? $"Database={_uniqueDatabaseName};"
                : $"{part};");
        }

        var uniqueDbTestConnectionString = uniqueDbTestConnectionStringBuilder.ToString().TrimEnd(';');
        return uniqueDbTestConnectionString;
    }

    public void Dispose()
    {
        using var dbContext = _serviceProvider.GetService<ChatRoomDbContext>();
        dbContext?.Database.EnsureDeleted();
    }
}