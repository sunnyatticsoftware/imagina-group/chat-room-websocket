using Chat.Application.Models;
using Chat.FunctionalTests.TestSupport;
using FluentAssertions;
using FluentAssertions.Execution;
using System.Collections.Concurrent;
using System.Net.Http.Json;
using System.Text.Json;
using TechTalk.SpecFlow;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Chat.FunctionalTests.StepDefinitions;

[Binding]
public class MessagesStepDefinition
    : FunctionalTest
{
    private Guid _chatRoomId;
    private IEnumerable<MessageDto> _messages = null!;
    private readonly IDictionary<string, WebsocketManager> _websocketManagers = new ConcurrentDictionary<string, WebsocketManager>();
    private readonly IDictionary<string, IList<string>> _userNotifications = new ConcurrentDictionary<string, IList<string>>();

    [Given(@"there is a chat room")]
    public async Task GivenThereIsAChatRoom()
    {
        var response = await HttpClient.PostAsync("api/chatrooms", null);
        var responseText = await response.Content.ReadAsStringAsync(); // e.g: "0e45de22-04f3-40e5-b6c9-93c277494a91"
        responseText = responseText.Replace("\"", string.Empty);
        var isValidGuid = Guid.TryParse(responseText, out var id);
        if (!isValidGuid)
        {
            throw new SpecFlowException("Invalid chat room id");
        }

        _chatRoomId = id;
    }

    [Given(@"a user with username (.*) and connection id (.*) joins")]
    public async Task GivenAUserWithUsernameAndConnectionIdJoins(string username, string connectionId)
    {
        await JoinUser(username, connectionId);
    }

    [Given(@"the user with username (.*) sends message (.*)")]
    public async Task GivenTheUserWithUsernameSendsMessage(string username, string text)
    {
        await SendMessage(username, text);
    }
    
    // Websocket
    [Given(@"a connected user with username (.*) joins")]
    public async Task GivenAConnectedUserWithUsernameJoins(string username)
    {
        var websocketManager = new WebsocketManager();
        _websocketManagers.Add(username, websocketManager);

        await websocketManager.Start(async () =>
            {
                // forces the websocket to reply with the connectionId
                await websocketManager.Send("ping");
            }, async connectionId =>
            {
                // joins the connected user to the chat
                await JoinUser(username, connectionId);
            },
            notificationJson =>
            {
                // appends received notification to user notifications
                var isExisting = _userNotifications.TryGetValue(username, out var notifications);
                if (!isExisting || notifications is null)
                {
                    notifications = new List<string>();
                }
                notifications.Add(notificationJson);
                _ = _userNotifications.TryAdd(username, notifications);
                
                return Task.CompletedTask;
            });
        await Task.Delay(2000);
    }
    
    [When(@"the user with username (.*) sends message (.*)")]
    public async Task WhenTheUserWithUsernameSendsMessage(string username, string text)
    {
        await SendMessage(username, text);
    }

    [When(@"getting all messages")]
    public async Task WhenGettingAllMessages()
    {
        var response = await HttpClient.GetAsync($"api/chatrooms/{_chatRoomId}/messages");
        
        var json = await response.Content.ReadAsStringAsync();
        _messages = JsonSerializer.Deserialize<IEnumerable<MessageDto>>(json,
            new JsonSerializerOptions(JsonSerializerDefaults.Web))!;
    }

    [Then(@"the message (.*) is from (.*) and says (.*)")]
    public void ThenTheMessageIsFromAndSays(int position, string username, string text)
    {
        var index = position - 1;
        var message = _messages.ElementAt(index);
        message.CreatedBy.Should().Be(username);
        message.Text.Should().Be(text);
    }

    [Then(@"the the user with username (.*) receives message containing (.*)")]
    public async Task ThenTheTheUserWithUsernameReceivesMessageContaining(string username, string message)
    {
        await Task.Delay(2000);
        var hasWebsocket = _websocketManagers.TryGetValue(username, out var websocketManager);
        if (!hasWebsocket || websocketManager is null)
        {
            throw new AssertionFailedException($"The {username} has no websocket");
        }
        
        var hasNotifications = _userNotifications.TryGetValue(username, out var notifications);
        if (!hasNotifications || notifications is null || !notifications.Any())
        {
            throw new AssertionFailedException($"The user {username} has no notifications");
        }

        var lastNotification = notifications.Last();
        lastNotification.Should().Contain(message);
        
        await websocketManager.Close();
    }

    private async Task JoinUser(string username, string connectionId)
    {
        var userPayload =
            new
            {
                Username = username,
                ConnectionId = connectionId
            };

        var response = await HttpClient.PostAsJsonAsync($"api/chatrooms/{_chatRoomId}/users", userPayload);
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception($"Unexpected http response {response.StatusCode} {response.ReasonPhrase}");
        }
    }

    private async Task SendMessage(string username, string text)
    {
        var response = await HttpClient.PostAsJsonAsync($"api/chatrooms/{_chatRoomId}/users/{username}/messages", text);
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception($"Unexpected http response {response.StatusCode} {response.ReasonPhrase}");
        }
    }
}