using Chat.FunctionalTests.TestSupport;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace Chat.FunctionalTests.StepDefinitions;

[Binding]
public class ChatRoomStepDefinitions
    : FunctionalTest
{
    private HttpResponseMessage _result = null!;

    [When(@"creating a chat room")]
    public async Task WhenCreatingAChatRoom()
    {
        _result = await HttpClient.PostAsync("api/chatrooms", null);
    }

    [Then(@"the chat room should have a valid id")]
    public async Task ThenTheChatRoomShouldHaveAValidId()
    {
        var responseText = await _result.Content.ReadAsStringAsync(); // e.g: ""<id>""
        responseText = responseText.Replace("\"", string.Empty);
        var isValidId = Guid.TryParse(responseText, out var id);
        isValidId.Should().BeTrue();
    }
}