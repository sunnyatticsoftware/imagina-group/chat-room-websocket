using Chat.FunctionalTests.TestSupport;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace Chat.FunctionalTests.StepDefinitions;

[Binding]
public class HealthStepDefinitions
    : FunctionalTest
{
    private string _urlPath = string.Empty;
    private HttpResponseMessage _result = null!;

    [Given(@"the health url (.*)")]
    public void GivenTheHealthUrl(string urlPath)
    {
        _urlPath = urlPath;
    }

    [When(@"sending GET request")]
    public async Task WhenSendingGetRequest()
    {
        _result = await HttpClient.GetAsync(_urlPath);
    }

    [Then(@"the response has status code (.*)")]
    public void ThenTheResponseHasStatusCode(int statusCode)
    {
        var httpStatusCode = (int) _result.StatusCode;
        httpStatusCode.Should().Be(statusCode);
    }

    [Then(@"the executing environment is (.*)")]
    public async Task ThenTheExecutingEnvironmentIsTest(string environment)
    {
        var payload = await _result.Content.ReadAsStringAsync();
        payload.Should().Contain($"Environment={environment}");
    }
}