Feature: Get Real Time Notifications
	Gets real time notifications through websocket

Scenario: Users connected to websocket and joined receive real time notifications
	Given there is a chat room
	And a connected user with username pepe joins
	And a connected user with username paula joins
	And a connected user with username leyre joins
	When the user with username leyre sends message Hello how is everyone?
	Then the the user with username leyre receives message containing leyre says: Hello how is everyone?
