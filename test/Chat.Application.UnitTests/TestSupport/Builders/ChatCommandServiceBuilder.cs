using Chat.Application.Contracts;
using Moq;

namespace Chat.Application.UnitTests.TestSupport.Builders;

public class ChatCommandServiceBuilder
{
    private IChatRoomRepository _chatRoomRepository = Mock.Of<IChatRoomRepository>();
    private IChatRoomFactory _chatRoomFactory = Mock.Of<IChatRoomFactory>();

    public ChatCommandServiceBuilder WithChatRoomRepository(IChatRoomRepository chatRoomRepository)
    {
        _chatRoomRepository = chatRoomRepository;
        return this;
    }

    public ChatCommandServiceBuilder WithChatRoomFactory(IChatRoomFactory chatRoomFactory)
    {
        _chatRoomFactory = chatRoomFactory;
        return this;
    }

    public ChatCommandService Object => new ChatCommandService(_chatRoomRepository, _chatRoomFactory);
}