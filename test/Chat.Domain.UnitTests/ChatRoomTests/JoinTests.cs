using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Builders;
using FluentAssertions;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class JoinTests
{
    public class Should_Join_Users
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user = null!;
        private Exception _exception = null!;

        protected override void Given()
        {
            _user = new User("foo", "bar");
            _sut =
                new ChatRoomBuilder()
                    .Object;
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Any_Exception()
        {
            _exception.Should().BeNull();
        }
    }
    
    public class Should_Not_Join_Already_Existing_User
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user = null!;
        private Exception _exception = null!;
        private string _expectedExceptionMessage = null!;

        protected override void Given()
        {
            _user = new User("foo", "bar");
            _expectedExceptionMessage = "User with username foo and connection Id bar already exists";
            _sut =
                new ChatRoomBuilder()
                    .Object;

            var existingUser = new User(_user.Username, _user.ConnectionId);
            _sut.Join(existingUser);
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Throw_An_Exception()
        {
            _exception.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Be_An_ArgumentException()
        {
            _exception.Should().BeAssignableTo<ArgumentException>();
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_Exception_Message()
        {
            _exception.Message.Should().Be(_expectedExceptionMessage);
        }
    }
    
    public class Should_Update_Connection_Id_For_Existing_Username
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user = null!;
        private Exception _exception = null!;

        protected override void Given()
        {
            _user = new User("foo", "bar");
            _sut =
                new ChatRoomBuilder()
                    .Object;

            var existingUser = new User(_user.Username, "old");
            _sut.Join(existingUser);
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_An_Exception()
        {
            _exception.Should().BeNull();
        }
    }
}