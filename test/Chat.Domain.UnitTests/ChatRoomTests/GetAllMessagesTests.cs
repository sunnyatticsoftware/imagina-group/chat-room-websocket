using Chat.Domain.Contracts;
using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Builders;
using FluentAssertions;
using Moq;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class GetAllMessagesTests
{
    public class Should_Get_All_Messages_Sent
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private IEnumerable<Message> _result = null!;
        private IEnumerable<Message> _expectedMessages = null!;

        protected override void Given()
        {
            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            var dateOne = new DateTime(2022, 1,1, 1,1,1);
            var dateTwo = new DateTime(2022, 1,1,1,1,2);

            dateTimeFactoryMock
                .SetupSequence(x => x.CreateUtcNow())
                .Returns(dateOne)
                .Returns(dateTwo);

            _sut =
                new ChatRoomBuilder()
                    .WithDateTimeFactory(dateTimeFactoryMock.Object)
                    .Object;

            var userOne = new User("foo", "a");
            var userTwo = new User("bar", "b");
            
            _sut.Join(userOne);
            _sut.Join(userTwo);
            
            _sut.SendMessage(userOne.Username, "hello");
            _sut.SendMessage(userTwo.Username, "bye");

            _expectedMessages =
                new List<Message>
                {
                    new(dateOne, "foo", "hello"),
                    new(dateTwo, "bar", "bye")
                };
        }

        protected override void When()
        {
            _result = _sut.GetAllMessages();
        }

        [Fact]
        public void Then_It_Should_Return_A_Valid_Enumerable_Of_Messages()
        {
            _result.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Return_The_Expected_Messages()
        {
            _result.Should().BeEquivalentTo(_expectedMessages);
        }
    }
    
    public class Should_Get_All_Messages_Sent_In_Order
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private IEnumerable<Message> _result = null!;
        private IEnumerable<Message> _expectedMessages = null!;

        protected override void Given()
        {
            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            var dateOne = new DateTime(2022, 1,1, 1,1,1);
            var dateTwo = new DateTime(2022, 1,1,1,1,2);
            var dateThree = new DateTime(2022, 1,1,1,1,3);

            dateTimeFactoryMock
                .SetupSequence(x => x.CreateUtcNow())
                .Returns(dateOne)
                .Returns(dateTwo)
                .Returns(dateThree);

            _sut =
                new ChatRoomBuilder()
                    .WithDateTimeFactory(dateTimeFactoryMock.Object)
                    .Object;

            var userOne = new User("foo", "a");
            var userTwo = new User("bar", "b");
            var userThree = new User("biz", "c");
            
            _sut.Join(userOne);
            _sut.Join(userTwo);
            _sut.Join(userThree);
            
            _sut.SendMessage(userOne.Username, "hello");
            _sut.SendMessage(userTwo.Username, "bye");
            _sut.SendMessage(userThree.Username, "ok then");

            _expectedMessages =
                new List<Message>
                {
                    new(dateOne, "foo", "hello"),
                    new(dateTwo, "bar", "bye"),
                    new(dateThree, "biz", "ok then")
                };
        }

        protected override void When()
        {
            _result = _sut.GetAllMessages();
        }

        [Fact]
        public void Then_It_Should_Return_A_Valid_Enumerable_Of_Messages()
        {
            _result.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Return_The_Expected_Messages()
        {
            _result.Should().BeEquivalentTo(_expectedMessages, config => config.WithStrictOrdering());
        }
    }
    
}