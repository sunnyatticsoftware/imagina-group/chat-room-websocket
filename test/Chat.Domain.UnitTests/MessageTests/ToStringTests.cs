using Chat.Domain.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Domain.UnitTests.MessageTests;

public static class ToStringTests
{
    public class Should_Display_Formatted_Message
        : Given_When_Then
    {
        private Message _sut = null!;
        private string _result = null!;
        private string _expectedFormattedMessage = "[2022-01-01 01:01:01] joe_bloggs says: Hello world";

        protected override void Given()
        {
            var createdOn = new DateTime(2022,1,1,1,1,1);
            var createdBy = "joe_bloggs";
            var text = "Hello world";
            _sut = new Message(createdOn, createdBy, text);
        }

        protected override void When()
        {
            _result = _sut.ToString()!;
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_Formatted_Message()
        {
            _result.Should().Be(_expectedFormattedMessage);
        }
    }
}